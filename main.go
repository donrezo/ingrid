package main

import (
	"context"
	"fmt"
	"ingrid/src/routing"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

const (
	Wait = time.Second * 15
	Host = "0.0.0.0:8080"
	WriteTimeout = time.Second * 15
	ReadTimeout = time.Second * 15
	IdleTimeout = time.Second * 60
)

func main()  {
	fmt.Println("Server start...")
	r := routing.Init()

	srv := &http.Server{
		Addr: Host,
		WriteTimeout: WriteTimeout,
		ReadTimeout:  ReadTimeout,
		IdleTimeout:  IdleTimeout,
		Handler: r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), Wait)
	defer cancel()

	srv.Shutdown(ctx)
	os.Exit(0)
}
