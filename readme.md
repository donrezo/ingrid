# Ingrid location api

This app is only for recruitment process, using in other cases not recomended.
# Install
```
git clone https://gitlab.com/donrezo/ingrid.git
cd ingrid
docker-compose -f docker-compose.local.yml up --build
```
  

You need also:
  - install and setup docker on your machine
  - For more inforamtion use official source: https://docs.docker.com/install/  
# Usage

Application work on port 8080 (host and port is configurable in app but if you want it change it dont forget change expose port in dockerfile).

For test you can use cli:
```
curl "0.0.0.0:8080/routes?src=13.388860,52.517037&dst=13.397634,52.529407&dst=13.428555,52.523219"
```

# Tests

To run basic tests:
```
cd ingrid
go test ./...
```



