package location

import (
	"reflect"
	"testing"
)

func TestCreateLocationRequestReturnTypeValidValue(t *testing.T) {
	destination := []string{"13.428554,52.523239","13.397631,52.529432"}
	source := "13.38886,52.517037"
	result := CreateLocationRequest(source, destination)

	expectedResult := Request{}

	if reflect.TypeOf(result) != reflect.TypeOf(&expectedResult) {
		t.Error(result)
	} else {
		t.Log(result)
	}
}

func TestGetRoutesReturnTypeEmptyValue(t *testing.T) {
	req := new(Request)
 	EmptyResult := GetRoutes(*req)
 	ExpectedResult := Response{}

 	if reflect.TypeOf(EmptyResult) != reflect.TypeOf(&ExpectedResult) {
		t.Error(EmptyResult)
	} else {
		t.Log(ExpectedResult)
	}
}
