package location

import (
	"ingrid/src/service/OSRM"
	"log"
	"strconv"
	"strings"
)

type Request struct {
	Src Src
	Dst []Dst
}

type Src struct {
	Src1, Src2 float64
}

type Dst struct {
	Dst1, Dst2 float64
}

type Response struct {
	Source []float64
	Routes []Routes
}

type Routes struct {
	Destination []float64
	Duration float64
	Distance float64
}


func GetRoutes(request Request) *Response{
	response := new(Response)
	response.Source = append(response.Source, request.Src.Src1)
	response.Source = append(response.Source, request.Src.Src2)

	for path := range request.Dst {
		res := fetchLocation(request.Src , request.Dst[path])
		data := readOSRM(*res)
		response.Routes = append(response.Routes, *data)
	}

	return response
}

func CreateLocationRequest(source string, destination []string)  *Request{
	request := new(Request)
	src, _ := transformToPoints(source, &Src{})
	request.Src = *src

	for path := range destination {
		_, dst := transformToPoints(destination[path], &Dst{})
		request.Dst = append(request.Dst, *dst)
	}

	return request
}

func transformToPoints(value string, t interface{}) (*Src,*Dst){
	s := strings.Split(value, ",")
	a, b := s[0], s[1]

	switch t.(type) {
	case *Dst:
		val := new(Dst)
		val.Dst1 = convertToFloat(a)
		val.Dst2 = convertToFloat(b)
		return nil, val
	case *Src:
		val := new(Src)
		val.Src1 = convertToFloat(a)
		val.Src2 = convertToFloat(b)
		return val , nil
	}
	return nil,nil
}

func convertToFloat(value string) float64{
	
	floatvalue, err := strconv.ParseFloat(value,64)
	if err != nil {
		log.Panic(err)
	}

	return floatvalue
}

func fetchLocation(source Src, destination Dst) *OSRM.Response {
	req := new(OSRM.Request)
	req.SRC1 = source.Src1
	req.SRC2 = source.Src2
	req.DST1 = destination.Dst1
	req.DST2 = destination.Dst2
	response := OSRM.GetLocationInfo(*req)

	return response
}

func readOSRM(response OSRM.Response) *Routes {
	data := new(Routes)

	for value := range response.Routes {
		data.Duration = response.Routes[value].Distance
		data.Distance = response.Routes[value].Duration
	}

	for value2 := range response.Waypoints {
		data.Destination = response.Waypoints[value2].Location
	}

	return data
}