package OSRM

import (
	"reflect"
	"testing"
)

func TestGetLocationInfo(t *testing.T) {
	req := new(Request)
	req.DST1 = 13.397631
	req.DST2 = 52.529432
	req.SRC1 = 13.38886
	req.SRC2 = 52.517037
	result := GetLocationInfo(*req)

	expectedResult := Response{}

	if reflect.TypeOf(result) != reflect.TypeOf(&expectedResult) {
		t.Error(result)
	} else {
		t.Log(result)
	}
}