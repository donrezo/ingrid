package OSRM

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

var myClient = &http.Client{Timeout: 10 * time.Second}

type Response struct {
	Routes []Routes `json:"routes,omitempty"`
	Waypoints []Waypoints `json:"waypoints,omitempty"`
	Message json.RawMessage `json:"message,omitempty"`
}

type Routes struct {
	Distance float64 `json:"distance,omitempty"`
	Duration float64 `json:"duration,omitempty"`
}

type Waypoints struct {
	Location []float64 `json:"location"`
}

type Request struct {
	SRC1 float64
	SRC2 float64
	DST1 float64
	DST2 float64
}


func callApi(url string,response interface{}) error{
	res, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	return json.NewDecoder(res.Body).Decode(&response)
}

func GetLocationInfo(request Request) *Response {

  url := buildLink(request)
  response := new(Response)
  err := callApi(url, &response)
  if err != nil {
	log.Panic(err)
  }

  return response
}

func convertToString(float float64) string{
	string := fmt.Sprintf("%f", float)
	return string
}

func buildLink(request Request) string{
	semi := ","
	coma := ";"
	base := "http://router.project-osrm.org/route/v1/driving/"
	end := "?overview=false"
	src1 := convertToString(request.SRC1)
	src2 := convertToString(request.SRC2)
	dst1 := convertToString(request.DST1)
	dst2 := convertToString(request.DST2)

	url := base + src1 + semi + src2 + coma + dst1 + semi + dst2 + end

	return url
}
