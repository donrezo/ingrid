package routing

import (
	"github.com/gorilla/mux"
	"net/http"
	"ingrid/src/controller"
)

func Init() (*mux.Router) {
	r := mux.NewRouter()
	r.HandleFunc("/routes", controller.Driving)
	http.Handle("/",r)

	return r
}
