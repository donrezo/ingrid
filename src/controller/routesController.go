package controller

import (
	"encoding/json"
	"ingrid/src/service/location"
	"log"
	"net/http"
)

func Driving(w http.ResponseWriter, r *http.Request)  {
	source := r.URL.Query().Get("src")
	destination, ok := r.URL.Query()["dst"]

	if !ok || len(destination[0]) < 1 {
		log.Println("Url Param 'key' is missing")
		return
	}

	req := location.CreateLocationRequest(source, destination)
	response := location.GetRoutes(*req)

	JSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(JSON)
}

